module gitlab.switch.ch/ub-unibas/alma2elasticrest

go 1.21

// replace gitlab.switch.ch/ub-unibas/dlza/digispace => gitlab.switch.ch/ub-unibas/dlza/digispace.git v0.0.6

replace gitlab.switch.ch/ub-unibas/dlza/metsparser => gitlab.switch.ch/ub-unibas/dlza/metsparser.git v0.0.6

replace gitlab.switch.ch/ub-unibas/dlza/filepathfrommets => gitlab.switch.ch/ub-unibas/dlza/filepathfrommets.git v0.0.4

require (
	github.com/andybalholm/brotli v1.1.0
	github.com/dgraph-io/badger/v3 v3.2103.5
	github.com/jinzhu/configor v1.2.2
	// gitlab.switch.ch/ub-unibas/alma2elastic/v2 v2.1.1
	gitlab.switch.ch/ub-unibas/dlza/filepathfrommets v0.0.4
	gitlab.switch.ch/ub-unibas/dlza/metsparser v0.0.6
	gitlab.switch.ch/ub-unibas/go-ublogger v0.0.0-20240123164453-4e831d61b3dd
)

require gitlab.switch.ch/ub-unibas/alma2elastic/v2 v2.0.0-20231117100957-3cd89ca41ee7

require (
	emperror.dev/emperror v0.33.0 // indirect
	emperror.dev/errors v0.8.1 // indirect
	github.com/BurntSushi/toml v1.3.2 // indirect
	github.com/cespare/xxhash v1.1.0 // indirect
	github.com/cespare/xxhash/v2 v2.2.0 // indirect
	github.com/dgraph-io/ristretto v0.1.1 // indirect
	github.com/dustin/go-humanize v1.0.1 // indirect
	github.com/gogo/protobuf v1.3.2 // indirect
	github.com/golang/glog v1.2.0 // indirect
	github.com/golang/groupcache v0.0.0-20210331224755-41bb18bfe9da // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/golang/snappy v0.0.4 // indirect
	github.com/google/flatbuffers v23.5.26+incompatible // indirect
	github.com/je4/utils/v2 v2.0.17 // indirect
	github.com/klauspost/compress v1.17.4 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/rs/zerolog v1.31.0 // indirect
	github.com/telkomdev/go-stash v1.0.3 // indirect
	gitlab.switch.ch/ub-unibas/gndservice/v2 v2.0.4 // indirect
	go.opencensus.io v0.24.0 // indirect
	go.uber.org/multierr v1.11.0 // indirect
	golang.org/x/exp v0.0.0-20231226003508-02704c960a9b // indirect
	golang.org/x/net v0.19.0 // indirect
	golang.org/x/sys v0.15.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	google.golang.org/protobuf v1.32.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
	rogchap.com/v8go v0.9.0 // indirect
)
