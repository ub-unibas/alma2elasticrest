FROM golang:1.21 as builder

WORKDIR /alma2elasticrest

ARG SSH_PUBLIC_KEY=$SSH_PUBLIC_KEY
ARG SSH_PRIVATE_KEY=$SSH_PRIVATE_KEY

ENV GO111MODULE=on
ENV GOPRIVATE=gitlab.switch.ch/ub-unibas/*
ENV CGO_ENABLED=1
ENV GOOS=linux
ENV GOARCH=amd64

COPY . .

RUN apt-get update && \
    apt-get install -y \
        git \
        openssh-client \
        ca-certificates

RUN eval $(ssh-agent -s)
RUN mkdir -p ~/.ssh
RUN chmod 700 ~/.ssh
# #for CI/CD build
RUN echo "$SSH_PRIVATE_KEY" | base64 -d >> ~/.ssh/id_rsa
# #for local build
# RUN echo "$SSH_PRIVATE_KEY" >> ~/.ssh/id_rsa


RUN echo "$SSH_PUBLIC_KEY" | tr -d '\r'   >> ~/.ssh/authorized_keys

RUN chmod 600 ~/.ssh/id_rsa
RUN chmod 644 ~/.ssh/authorized_keys
RUN ssh-keyscan gitlab.switch.ch >> ~/.ssh/known_hosts
RUN chmod 644 ~/.ssh/known_hosts
# local build
RUN git config --global url."ssh://git@gitlab.switch.ch/".insteadOf "https://gitlab.switch.ch/"
# #for CI/CD build
# RUN git config --global --add url."https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.switch.ch".insteadOf "https://gitlab.switch.ch"

RUN go mod download
RUN go build 
ENTRYPOINT ["./alma2elasticrest", "--config", "/configs/a2erest.toml"]

# FROM alpine
# WORKDIR /
# RUN apk upgrade --no-cache && \
#     apk add --no-cache  bash openssl libgcc libstdc++ ncurses-libs

# COPY --from=builder /alma2elasticrest /
# # EXPOSE 8080

# ENTRYPOINT ["./alma2elasticrest", "--config", "/configs/a2erest.toml"]