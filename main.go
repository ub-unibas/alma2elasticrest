package main

import (
	"bytes"
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"encoding/pem"
	"encoding/xml"
	"flag"
	"fmt"
	"io"
	"io/fs"
	"log"
	"net/http"
	"time"

	"strings"

	// "encoding/xml"

	"emperror.dev/emperror"
	"emperror.dev/errors"
	"github.com/andybalholm/brotli"
	"github.com/dgraph-io/badger/v3"
	"gitlab.switch.ch/ub-unibas/alma2elasticrest/config"
	"gitlab.switch.ch/ub-unibas/alma2elasticrest/data/certs"

	"gitlab.switch.ch/ub-unibas/alma2elastic/v2/pkg/marc21"
	filepathmodels "gitlab.switch.ch/ub-unibas/dlza/filepathfrommets/models"
	metsparser "gitlab.switch.ch/ub-unibas/dlza/metsparser"
	metsparsermodels "gitlab.switch.ch/ub-unibas/dlza/metsparser/models"
	ub_logger "gitlab.switch.ch/ub-unibas/go-ublogger"
)

type MediaserverResponse struct {
	Val MediaObject `json:"Val,omitempty"`
}
type MediaObject struct {
	Type        string `json:"type,omitempty"`        // from mediaserver
	MimeType    string `json:"mimetype,omitempty"`    // from mediaserver
	Width       int64  `json:"width,omitempty"`       // from mediaserver
	Height      int64  `json:"height,omitempty"`      // from mediaserver
	Orientation int64  `json:"orientation,omitempty"` // from mediaserver
	Duration    int64  `json:"duration,omitempty"`    // from mediaserver
	Label       string `json:"label,omitempty"`       // label from mets file Physical structMap
	Name        string `json:"name,omitempty"`        // created from mets
	Uri         string `json:"uri,omitempty"`         //mediaserver:collection/signature
	Promom      string `json:"pronom,omitempty"`      //from mediaserver https://www.nationalarchives.gov.uk/PRONOM/Format/proFormatSearch.aspx?status=detailReport&id=1264
	Internal    bool   `json:"internal,omitempty"`
}

// 1 media per xml files
type Media struct {
	Poster *MediaObject   `json:"poster,omitempty"`
	Medias []*MediaObject `json:"medias,omitempty"`
}

type RequestAPIFields struct {
	Url     string `json:"url,omitempty"`
	IsLocal bool   `json:"is_local,omitempty"`
}

// type DOIinput struct {
// 	Field map[string]map[string]interface{} `json:"field,omitempty"`
// }

// type SubSubFields struct {
// 	A  string `json:"a,omitempty"`
// 	A2 string `json:"2,omitempty"`
// }

// type Subfield struct {
// 	List []SubSubFields `json:"subsub,omitempty"`
// }
// type f024 struct {
// 	Ind1      string `json:"ind1,omitempty"`
// 	Ind2      string `json:"ind2,omitempty"`
// 	Subfields []map[string]interface{}
// }
// type Field struct {
// 	F024 f024 `json:"024,omitempty"`
// }

// type DOIinput struct {
// 	Field Field `json:"field,omitempty"`
// }

type DataStructure struct {
	Media     filepathmodels.Media `json:"media,omitempty"`
	Structure Structure            `json:"structure,omitempty"`
}

type Structure struct {
	DigitalObject metsparsermodels.DigitalObject `json:"digital_object,omitempty"`
}

type Data struct {
	Media         filepathmodels.Media           `json:"media,omitempty"`
	DigitalObject metsparsermodels.DigitalObject `json:"digital_object,omitempty"`
}

type BadgerDB struct {
	Db     *badger.DB
	Logger *ub_logger.Logger
}

// type FilePath struct {
// 	Identifier string `json:"identifier,omitempty"`
// 	Collection string `json:"collection,omitempty"`
// 	Path       string `json:"path,omitempty"`
// }

const (
	LogFile   = ""
	LogFormat = `%{time:2006-01-02T15:04:05.000} %{module}::%{shortfunc} [%{shortfile}] > %{level:.5s} - %{message}`
)

var addr = flag.String("addr", "localhost:12356", "address to listen to")
var configParam = flag.String("config", "", "config file in toml format")
var configType = flag.String("filetype", "", "config file format")

func (badgerDB *BadgerDB) MetsRestHTTP(writer http.ResponseWriter, request *http.Request) {

	writer.Header().Set("Content-Type", "application/json")
	logger := badgerDB.Logger
	logger.Infof("handler called from '%s'", request.RemoteAddr)
	tmpLogger := logger.With().Str("func_name", "MetsRestHTTP").Logger()
	sub_logger := &ub_logger.Logger{&tmpLogger}
	bodyData, err := io.ReadAll(request.Body)
	request.Body.Close()
	if err != nil {
		logger.Errorf("cannot load request body: %v", err)
		http.Error(writer, fmt.Sprintf("cannot load request body: %v", err), http.StatusBadRequest)
		return
	}

	var content *marc21.QueryStructMARCIJ

	// fmt.Println("\n\nbodyData", bodyData)
	if err := json.Unmarshal(bodyData, &content); err != nil {
		logger.Errorf("cannot unmarshal request body: %v", err)
		logger.Debugf("\n%s", string(bodyData))
		http.Error(writer, fmt.Sprintf("cannot unmarshal request body: %v", err), http.StatusBadRequest)
		return
	}
	fmt.Println("\n\ncontent", content.Object.Fields)
	//legacyID is the alma ID, it is the link with the mets datei RecordIdentifier
	// AlmaID := ""
	DOI := ""
	SysNum := ""

	for _, object := range content.Object.Fields {
		if object.Code == "001" { // retrieve AlmaId
			// AlmaID = object.Text
		} else if object.Code == "024" { // retrieve DOI

			if object.Subfields[1]["2"] == "doi" {
				DOI = object.Subfields[0]["a"]
			}
		} else if object.Code == "035" { //retrieve LegacyID/System Number

			if strings.Contains(object.Subfields[0]["a"], "DSV") {
				// SysNum = strings.Split(strings.Split(object.Subfields[0]["a"], "DSV")[0], ")")[1]
				SysNum = strings.Split(object.Subfields[0]["a"], ")")[1]
			}
		}
	}

	logger.Debugf(" DOI = %v, SysNum = %v\n", DOI, SysNum)
	var jsonStr filepathmodels.Data

	err = badgerDB.Db.View(func(txn *badger.Txn) error {
		//Get Item in badger DB with DOI
		item, err := txn.Get([]byte(DOI))
		if err != nil {
			logger.Errorf("Get error no DOI match: %v", err)
			//Get Item in badger DB with SysNum
			item, err = txn.Get([]byte(SysNum))
			if err != nil {
				logger.Errorf("Get error no DOI : %v nor SysNum :%v match : %v", DOI, SysNum, err)
				// add something to elastic search for log purpose
				data := map[string]string{
					"log_type":  "No DOI nor SysNum corresponding to a2e input",
					"doi":       DOI,
					"sysnum":    SysNum,
					"datetime":  time.Now().String(),
					"log_level": "error",
					"error":     err.Error(),
				}
				sub_logger.Warn().Interface("data", data).Msg("No DOI nor SysNum corresponding to a2e input " + time.Now().String() + err.Error())
				return nil
			}
		}

		var valCopy []byte

		// Alternatively, you could also use item.ValueCopy().
		valCopy, err = item.ValueCopy(nil)
		if err != nil {
			logger.Errorf("valCopy error : %v", err)
		}
		reader := bytes.NewReader(valCopy)
		breader := brotli.NewReader(reader)

		respBody, err := io.ReadAll(breader)
		if err != nil {
			logger.Errorf("error decoding br response : %v", err)
		}

		err = json.Unmarshal(respBody, &jsonStr)
		logger.Debugf(" value=%v\n", jsonStr)
		if err != nil {
			logger.Errorf("error json.Unmarshal : %v", err)
			return err
		}

		return nil
	})
	if err != nil {
		logger.Fatalf("db view error : %v", err)
	}

	//set url path to call correct e-portal URL to process if intermal image or not
	urlpath := ""
	if strings.Contains(jsonStr.DigitalObject.Header.Instance, "rara") {
		urlpath = "https://www.e-rara.ch/oai?verb=GetRecord&metadataPrefix=mets&identifier=oai:www.e-rara.ch:" + strings.ReplaceAll(jsonStr.DigitalObject.ID, "md", "")
	} else if strings.Contains(jsonStr.DigitalObject.Header.Instance, "manuscripta") {
		urlpath = "https://www.e-manuscripta.ch/oai?verb=GetRecord&metadataPrefix=mets&identifier=oai:www.e-manuscripta.ch:" + strings.ReplaceAll(jsonStr.DigitalObject.ID, "md", "")
	}
	if urlpath == "" {
		logger.Errorf("no eportal result found %v", jsonStr)
		return
	}
	metsFile := RequestMetsFile(urlpath, logger)
	var record metsparsermodels.Record
	record.Metadata.Mets = metsFile

	digicollec, err := metsparser.MetsParser(&record, logger)
	if err != nil {
		logger.Errorf(" metsparser.MetsParser error : %v", err)
	}

	//Compare retrieve mets file with data stored in Data
	badgerDigiCollec := jsonStr.DigitalObject
	// badgerDigiMedias := jsonStr.Media.Medias

	//Set internal for first Occurences of Files in first layer level
	for _, bFile := range badgerDigiCollec.Files {
		for _, file := range digicollec.Files {
			if bFile.Href == file.Href {
				bFile.Internal = false
				break
			} else {
				bFile.Internal = true
			}
		}
	}

	//Set internal recursive call for lower level files
	hreflist := GetHrefList(digicollec.DigitalContainer)
	SetInternal(badgerDigiCollec.DigitalContainer, hreflist)
	// SetInternalmedia(badgerDigiMedias, hreflist)

	// resultData, err := json.MarshalIndent(jsonStr, "", "   ")
	resultData, err := json.MarshalIndent(DataStructure{
		Media: *jsonStr.Media,
		Structure: Structure{
			DigitalObject: jsonStr.DigitalObject,
		},
	}, "", "   ")

	if err != nil {
		logger.Errorf("cannot marshal result: %v", err)
		http.Error(writer, fmt.Sprintf("cannot marshal result: %v", err), http.StatusInternalServerError)
		return
	}
	logger.Infof("OK")
	writer.WriteHeader(http.StatusOK)
	io.WriteString(writer, string(resultData))
}

func router(db *badger.DB, logger *ub_logger.Logger) http.Handler {
	mux := http.NewServeMux()

	badgerDB := &BadgerDB{Db: db, Logger: logger}
	mux.HandleFunc("/", badgerDB.MetsRestHTTP)

	return mux
}

func main() {
	flag.Parse()
	// // create logger instance
	// namespace := "filepathfrommets"
	// var host string = "sb-uwf4.swissbib.unibas.ch"
	// var port = 5046

	config, err := config.GetConfig(*configParam, *configType)
	if err != nil {
		log.Fatal(err)
	}

	logger, logStash, logFile := ub_logger.CreateUbMultiLoggerTLS(
		config.Logging.TraceLevel, config.Logging.Filename,
		ub_logger.SetLogStash(config.Logging.StashHost,
			config.Logging.StashPortNb,
			config.Logging.Namespace,
			config.Logging.StashTraceLevel))
	if logStash != nil {
		defer logStash.Close()
	}
	if logFile != nil {
		defer logFile.Close()
	}

	if config.Addr != "" {
		addr = &config.Addr
	}
	// traceLevel := "InfoLevel"

	// logF := "log_Alma2Elastic_" + time.Now().Format("20060102") + ".log"

	// tlsConfig := &tls.Config{}
	// // logger, logStash, logFile := ub_logger.CreateUbMultiLoggerTLS(traceLevel, logF, ub_logger.SetLogStash(host, port, namespace, ""), ub_logger.SetTLS(true), ub_logger.SetTLSConfig(tlsConfig))

	// logger, logStash, logFile := ub_logger.CreateUbMultiLoggerTLS(traceLevel, logF, ub_logger.SetLogStash("", 0, "", ""), ub_logger.SetTLS(true), ub_logger.SetTLSConfig(tlsConfig))
	// // newLogger, logStash, logFile := ub_logger.CreateUbMultiLogger(host, port, traceLevel, "", logF, namespace)
	// // SetTLS(true), SetTLSConfig(tlsConfig)
	// if logStash != nil {
	// 	defer logStash.Close()
	// }
	// if logFile != nil {
	// 	defer logFile.Close()
	// }
	// opts := badger.DefaultOptions("/home/paulng/badgerDir/badgerMedia")
	opts := badger.DefaultOptions(config.Badger)
	opts.Logger = nil
	db, err := badger.Open(opts)
	if err != nil {
		log.Fatalf("cannot open badger DB error :%v", err)
	}
	// h2server := &http2.Server{IdleTimeout: time.Second * 60}
	// h2handler := h2c.NewHandler(router(db, logger), h2server)
	// server := &http.Server{
	// 	Addr:    *addr,
	// 	Handler: h2c.NewHandler(handler, h2s),
	// }
	var cert tls.Certificate
	var addCA = []*x509.Certificate{}

	certBytes, err := fs.ReadFile(certs.CertFS, "localhost.cert.pem")
	if err != nil {
		emperror.Panic(errors.Wrapf(err, "cannot read internal cert %v/%s", certs.CertFS, "localhost.cert.pem"))
	}
	keyBytes, err := fs.ReadFile(certs.CertFS, "localhost.key.pem")
	if err != nil {
		emperror.Panic(errors.Wrapf(err, "cannot read internal key %v/%s", certs.CertFS, "localhost.key.pem"))
	}
	if cert, err = tls.X509KeyPair(certBytes, keyBytes); err != nil {
		emperror.Panic(errors.Wrap(err, "cannot create internal cert"))
	}

	rootCABytes, err := fs.ReadFile(certs.CertFS, "ca.cert.pem")
	if err != nil {
		emperror.Panic(errors.Wrapf(err, "cannot read root ca %v/%s", certs.CertFS, "ca.cert.pem"))
	}
	block, _ := pem.Decode(rootCABytes)
	if block == nil {
		emperror.Panic(errors.Wrapf(err, "cannot decode root ca"))
	}
	rootCA, err := x509.ParseCertificate(block.Bytes)
	if err != nil {
		emperror.Panic(errors.Wrap(err, "cannot parse root ca"))
	}
	addCA = append(addCA, rootCA)

	rootCAs, _ := x509.SystemCertPool()
	if rootCAs == nil {
		rootCAs = x509.NewCertPool()
	}
	for _, ca := range addCA {
		rootCAs.AddCert(ca)
	}

	var tlsConfig = &tls.Config{
		Certificates:       []tls.Certificate{cert},
		RootCAs:            rootCAs,
		InsecureSkipVerify: config.ServiceInsecureSkipVerify,
	}

	server := http.Server{
		Addr:      *addr,
		Handler:   router(db, logger),
		TLSConfig: tlsConfig,
	}

	logger.Info().Msgf("Starting server : %s", *addr)
	if err := server.ListenAndServeTLS("", ""); err != nil {
		logger.Error().Msgf("server stopped: %v", err)
	} else {
		logger.Info().Msg("server shut down")
	}

	// logger.Fatalf(http.ListenAndServe(*addr, router(db, logger)).Error())
}

// RequestMetsFile call to e-portal to retrieve mets file
func RequestMetsFile(requestURL string, logger *ub_logger.Logger) metsparsermodels.Mets {

	//Data www.e-rara.ch:24921544 is too big, e-rara portal goes in timeout do i10.3931/e-rara-87727, SysNum = 007175157DSV01
	if requestURL == "https://www.e-rara.ch/oai?verb=GetRecord&metadataPrefix=mets&identifier=oai:www.e-rara.ch:24921544" {
		return metsparsermodels.Mets{}
	}
	response, err := http.Get(requestURL)
	if err != nil {
		logger.Fatalf(err.Error())
	}
	defer response.Body.Close()
	body, err := io.ReadAll(response.Body)
	if err != nil {
		fmt.Println("err  io.ReadAll: ", err)
		fmt.Println("io.ReadAll err requestURL", requestURL)
		logger.Fatalf(err.Error())
	}
	var oaipmhFile struct {
		GetRecord struct {
			Record metsparsermodels.Record `xml:"record"`
		} `xml:"GetRecord"`
	}
	err = xml.Unmarshal(body, &oaipmhFile)

	if err != nil {
		fmt.Println("unmarshal err requestURL", requestURL)
		fmt.Println("err RequestMetsFile: ", err)
		var test interface{}
		err = xml.Unmarshal(body, &test)
		fmt.Println("metsFile test:", test)
		logger.Fatalf(err.Error())
	}

	return oaipmhFile.GetRecord.Record.Metadata.Mets
}

// GetHrefList, list all href from a list of DigitalContainer
func GetHrefList(refContainers []*metsparsermodels.DigitalContainer) []string {
	var hreflist []string
	if hreflist == nil {
		hreflist = []string{}
	}
	for _, refcontainer := range refContainers {
		for _, file := range refcontainer.Files {
			if file != nil {
				hreflist = append(hreflist, file.Href)
			}
		}
		hreflist = append(hreflist, GetHrefList(refcontainer.DigitalContainer)...)

	}
	return hreflist
}

// GetHrefListMedia, list all MedisaServer href from a list of DigitalContainer
func GetHrefListMedia(refContainers []*metsparsermodels.DigitalContainer) []string {
	var hreflist []string
	if hreflist == nil {
		hreflist = []string{}
	}
	for _, refcontainer := range refContainers {
		for _, file := range refcontainer.Files {
			if file != nil {
				for htype, href := range file.HrefMap {
					if htype == "mediaserver" {
						hreflist = append(hreflist, href)
					}

				}

			}

		}
		hreflist = append(hreflist, GetHrefList(refcontainer.DigitalContainer)...)

	}
	return hreflist
}

// SetInternal reccursive call to know if file is both in mets zip file and e portal retrieved zip file
func SetInternal(containers []*metsparsermodels.DigitalContainer, hrefList []string) {
	for _, container := range containers {
		for _, bFile := range container.Files {
			bFile.Internal = true
			for _, href := range hrefList {
				if href == bFile.Href {
					bFile.Internal = false
					break
				}
			}
			if !bFile.Internal {
				break
			}

		}
		SetInternal(container.DigitalContainer, hrefList)
	}
}

// SetInternalmedia reccursive call to know if file is both in mets zip file and e portal retrieved zip file
func SetInternalmedia(containers []*filepathmodels.MediaObject, hrefList []string) {
	for _, container := range containers {
		container.Internal = true
		for _, href := range hrefList {
			fmt.Println("href:", href)
			fmt.Println("container.Uri:", container.Uri)
			fmt.Println("container.Label:", container.Label)
			fmt.Println("container.Path:", container.Path)
			if href == container.Uri {
				container.Internal = false
				break
			}
		}
		if !container.Internal {
			break
		}

	}
}
