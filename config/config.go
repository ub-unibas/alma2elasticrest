package config

import (
	"log"

	"github.com/jinzhu/configor"
)

type Config struct {
	Addr                      string        `yaml:"addr" toml:"addr"`
	Badger                    string        `yaml:"badger" toml:"badger"`
	Logging                   LoggingConfig `yaml:"logging" toml:"logging"`
	ServiceInsecureSkipVerify bool          `yaml:"skip_verify" toml:"skipVerify"`
}

// GetConfig creates a new config from a given environment
func GetConfig(configFile string, fileType string) (config Config, err error) {
	if configFile == "" {
		defaultConfig := "config.yml"
		if fileType == "toml" {
			defaultConfig = "config.toml"
		}
		err = configor.Load(&config, defaultConfig)
		if err != nil {
			log.Fatal(err)
		}
	} else {
		err = configor.Load(&config, configFile)
		if err != nil {
			log.Fatal(err)
		}
	}

	return
}
